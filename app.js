//jshint esversion:6
const express = require("express");
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require("path");
const port = process.env.PORT || "8000";
const ejs = require('ejs');
const session = require('express-session');
const passport = require('passport');
const passportLocalMongoose = require('passport-local-mongoose');

var app = express();

app.use(bodyParser.urlencoded({
  extended: true
}));
app.set('view engine', 'ejs');

// APP static files

app.use(express.static(__dirname + '/public'));

// Auth

app.set('trust proxy', 1);
app.use(session({
  secret: "secretforcrown",
  resave: false,
  saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());



var mongoDB = 'mongodb+srv://CrownCleaning:CrownCleaning@cluster0-gjbjd.mongodb.net/CrownCleaning?retryWrites=true&w=majority';
mongoose.connect(mongoDB, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
mongoose.set('useCreateIndex', true);

//Get the default connection
var db = mongoose.connection;

//DB schemas

var Schema = mongoose.Schema;

var offerSchema = new Schema({
  title: String,
  city: String,
  type: String,
  price: Number,
  description: String,
  workers: [{
    workerId: String
  }]
});

var Offer = mongoose.model('Offer', offerSchema);

var userSchema = new Schema({
  username: String,
  mail: String,
  password: String,
  offers: [{
    id: String
  }]
});

userSchema.plugin(passportLocalMongoose, {
  usernameField: 'mail'
});

var User = mongoose.model('User', userSchema);

passport.use(User.createStrategy());

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// **************
//  APP routes **
// **************

app.get('/', (req, res) => {
  var isLogged = req.isAuthenticated();

  Offer.find().sort({ _id: -1 }).limit(5).exec(function(err, result){
    if(isLogged) {
      res.render(__dirname + '/views/home.ejs', {isLogged: isLogged,
                                                 id: req.user._id,
                                                 offersArr: result});
    } else {
      res.render(__dirname + '/views/home.ejs', {isLogged: isLogged,
                                                 offersArr: result});
    }
  });
});

app.get('/error', (req, res) => {
  res.sendFile(__dirname + '/pages/error.html');
});

app.get('/login', (req, res) => {
  res.sendFile(__dirname + '/pages/login.html');
});

app.get('/register', (req, res) => {
  res.sendFile(__dirname + '/pages/register.html');
});

app.get('/secret', (req, res) => {
  if (req.isAuthenticated()) {
    res.send('hello');
  } else {
    res.redirect('/');
  }
});

app.get('/user/:id', (req, res) => {
  var id = req.params.id;
  User.findOne({_id: id}, (err, user) => {
    if(err) {
      console.log(err);
    } else {
      var isLogged = req.isAuthenticated();
      res.render(__dirname + '/views/user.ejs', {user: user, id: user._id, isLogged: isLogged});
    }
  });
});

app.post('/user/:id/username', (req, res) => {
  var id = req.params.id;
  var username = req.body.username;
  User.findByIdAndUpdate(id, {username: username}, (err) => {
    if(err) {
      console.log(err);
      res.sendFile(__dirname + '/pages/error.html');
    } else {
      res.redirect('/user/' + id);
    }
  });
});

app.post('/user/:id/mail', (req, res) => {
  var id = req.params.id;
  var username = req.body.mail;
  User.findByIdAndUpdate(id, {mail: mail}, (err) => {
    if(err) {
      res.sendFile(__dirname + '/pages/error.html');
    } else {
      res.redirect('/user/' + id);
    }
  });
});

app.post('/user/:id/password', (req, res) => {
  var id = req.params.id;
  var username = req.body.password;
  User.findByIdAndUpdate(id, {password: password}, (err) => {
    if(err) {
      res.sendFile(__dirname + '/pages/error.html');
    } else {
      res.redirect('/user/' + id);
    }
  });
});

// Routes for offers

app.post('/offer/add', (req, res) => {
  var newOffer = new Offer({
    title: req.body.title,
    description: req.body.description,
    city: req.body.city,
    price: req.body.price,
    type: req.body.type
  });

  newOffer.save((err, offer) => {
    if(err) {
      console.log(err);
      res.sendFile(__dirname + '/pages/error.html');
    } else {
      console.log(offer);
      User.findOneById(req.user._id, (err, user) => {
        var offerId = {offer: offer._id};
        user.offers.push(offerId);
        user.save((err) => {
          if(err) {
            console.log(err);
            res.sendFile(__dirname + '/pages/error.html');
          } else {
            res.redirect('/user/' + user._id);
          }
        });
      });
    }
  });
});

app.get('/offer/search/price', (req,res) => {
  var isLogged = req.isAuthenticated();
  Offer.find({price: req.body.price}, (err, result) => {
    if(err) {
      console.log(err);
      res.sendFile(__dirname + '/pages/error.html');
    } else {
      if(typeof result === Array) {
        res.render(__dirname + '/views/offers.ejs', {offersArr: result, message: 'znaleziono oferty', id: req.user._id, isLogged: isLogged});
      } else {
        res.render(__dirname + '/views/offers.ejs', {offersArr: result, message: 'nie znaleziono ofert', id: req.user._id, isLogged: isLogged});
      }
    }
  });
});

app.get('/offer/search/city', (req,res) => {
  var isLogged = req.isAuthenticated();
  Offer.find({city: req.body.city}, (err, result) => {
    if(err) {
      console.log(err);
      res.sendFile(__dirname + '/pages/error.html');
    } else {
      if(typeof result === Array) {
        res.render(__dirname + '/views/offers.ejs', {offersArr: result, message: 'znaleziono oferty', id: req.user._id, isLogged: isLogged});
      } else {
        res.render(__dirname + '/views/offers.ejs', {offersArr: result, message: 'nie znaleziono ofert', id: req.user._id, isLogged: isLogged});
      }
    }
  });
});

app.get('/offer/search/type', (req,res) => {
  var isLogged = req.isAuthenticated();
  Offer.find({type: req.body.type}, (err, result) => {
    if(err) {
      console.log(err);
      res.sendFile(__dirname + '/pages/error.html');
    } else {
      if(typeof result === Array) {
        res.render(__dirname + '/views/offers.ejs', {offersArr: result, message: 'znaleziono oferty', id: req.user._id, isLogged: isLogged});
      } else {
        res.render(__dirname + '/views/offers.ejs', {offersArr: result, message: 'nie znaleziono ofert', id: req.user._id, isLogged: isLogged});
      }
    }
  });
});
app.get('/offer/search/all', (req,res) => {
  var isLogged = req.isAuthenticated();
  Offer.find({city: req.body.city, type: req.body.type, price: req.body.price}, (err, result) => {
    if(err) {
      console.log(err);
      res.sendFile(__dirname + '/pages/error.html');
    } else {
      if(typeof result === Array) {
        res.render(__dirname + '/views/offers.ejs', {offersArr: result, message: 'znaleziono oferty', id: req.user._id, isLogged: isLogged});
      } else {
        res.render(__dirname + '/views/offers.ejs', {offersArr: result, message: 'nie znaleziono ofert', id: req.user._id, isLogged: isLogged});
      }
    }
  });
});

app.get('/offer/:id', (req, res) => {
  var isLogged = req.isAuthenticated();
  Offer.findOne({id: id}, (err, offer) => {
    if(err) {
      res.sendFile(__dirname + '/pages/error.html');
    } else {
      if(offer) {
        res.render(__dirname + '/views/offer.ejs',{ id: req.user._id, isLogged: isLogged, offer: offer});
      }
    }
  });
});

app.post('/offer/:id/work', (req, res) => {
  var offerId = req.params.id;
  User.findOneById(req.user._id, (err, user) => {
    if(err) {
      res.sendFile(__dirname + '/pages/error.html');
    } else {
      Offer.findOneById(offerId, (err, offer) => {
        if(err) {
          res.sendFile(__dirname + '/pages/error.html');
        } else {
          var workerId = {
            workerId: req.user._id
          };
          offer.workers.push(workerId);
        }
      });
    }
  });
});

app.post('/login', (req, res) => {
  const user = new User({
    mail: req.body.mail,
    password: req.body.password
  });

  req.login(user, (err) => {
    if (err) {
      console.log(err);
    } else {
      passport.authenticate('local')(req, res, function() {
        res.redirect('/');
      });
    }
  });
});

app.post('/register', (req, res) => {
  console.log(req.body);
  User.register({
    mail: req.body.mail,
    username: req.body.username
  }, req.body.password, (err, user) => {
    if (err) {
      console.log(err);
      res.redirect('/register');
    } else {
      passport.authenticate('local')(req, res, function() {
        res.redirect('/');
      });
    }
  });
});

// APP Listen

app.listen(port, () => {
  console.log('App is listening on port 8000');
});
